variable "access_key_id" {
}

variable "secret_access_key" {
}

variable "ssh_key" {
}

variable "db_user"{
  default  = "db_user"
}

variable "db_pass"{
  default  = "db_password"
}

variable "db_name"{
  default  = "realworld"
}

variable "db_port"{
  default  = "3306"
}

variable "db_url"{
  default  = "endpoint"
}

variable "region" {
  default  = "us-east-1"
}

variable "assume_role" {
  default = "arn:aws:iam::242906888793:role/AWS_Sandbox"
}

variable "session_name" {
  default = "AWS_Sandbox"
}

variable "vpc_network" {
  default = "10.10.0.0/16"
}

variable "public_subnets" {
  default = ["10.10.0.0/24", "10.10.1.0/24"]
}

variable "private_subnets" {
  default = ["10.10.2.0/24", "10.10.3.0/24"]
}

variable "db_subnets" {
  default = ["10.10.4.0/24", "10.10.5.0/24"]
}

variable "common_tags" {
  type = map(string)
  default = {
    project = "devops-training"
    client = "Coherent"    
    owner = "olegkurnitsov@coherentsolutions.com"
  }
}