output "s3_bucket_arn" {
  value       = aws_s3_bucket.okurnitsov_training_terraform_state.arn
  description = "The ARN of the S3 bucket"
}

output "dynamodb_table_name" {
  value       = aws_dynamodb_table.okurnitsov_training_terraform_locks.name
  description = "The name of the DynamoDB table"
}

output "bastion_host_ip_address" {
  value       = aws_instance.okurnitsov_bastion_instance.public_ip
  description = "The ip address of the bastion host"
}

output "backend_host_ip_address" {
  value       = aws_instance.okurnitsov_backend_instance.private_ip
  description = "The ip address of the backend host"
}

output "frontend_host_ip_address" {
  value       = aws_instance.okurnitsov_frontend_instance.private_ip
  description = "The ip address of the frontend host"
}

output "vars_for_frontend" {
  value       = data.template_file.frontend.vars
  description = "The vars for frontend host"
}

output "vars_for_backend" {
  value       = data.template_file.backend.vars
  description = "The vars for backend host"
}