### --- === Security groups === --- ###

resource "aws_security_group" "okurnutsov_external_ssh_access" {
  name          = "ssh-access-from-minsk"
  description   = "Allow SSH traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["104.59.125.84/32", "216.70.7.11/32", "80.94.174.82/32", "82.209.242.80/29", "86.57.155.180/32", "86.57.158.250/32"]
  }
  egress {
    from_port   = 0 #Allow all traffic
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-external-ssh-access"
                  ))
}

resource "aws_security_group" "okurnutsov_internal_ssh_access" {
  name          = "ssh-access"
  description   = "Allow SSH traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_vpc.cidr_block]
  }
  egress {
    from_port   = 0 #Allow all traffic
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-internal-ssh-access"
                  ))
}

resource "aws_security_group" "okurnutsov_web_access" {
  name          = "okurnitsov-web-access"
  description   = "Allow web traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0 
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-web-access"
                  ))
}

resource "aws_security_group" "okurnutsov_db_access" {
  name          = "okurnitsov-db-access"
  description   = "Allow db traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc.id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_vpc.cidr_block]
  }
  egress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_vpc.cidr_block]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-db-access"
                  ))
}

resource "aws_security_group" "okurnutsov_icmp_access" {
  name          = "okurnitsov-icmp-access"
  description   = "Allow icmp traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc.id

  ingress {
    from_port   = "-1"
    to_port     = "-1"
    protocol    = "icmp"
    cidr_blocks = [aws_vpc.okurnitsov_vpc.cidr_block]
  }
  egress {
    from_port   = "-1"
    to_port     = "-1"
    protocol    = "icmp"
    cidr_blocks = [aws_vpc.okurnitsov_vpc.cidr_block]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-icmp-access"
                  ))
}

resource "aws_security_group" "okurnutsov_frontend_access" {
  name          = "okurnitsov-frontend-access"
  description   = "Allow frontend traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc.id

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-frontend-access"
                  ))
}

resource "aws_security_group" "okurnutsov_backend_access" {
  name          = "okurnitsov-backend-access"
  description   = "Allow backend traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc.id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-backend-access"
                  ))
}