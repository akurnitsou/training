### This repository for training ###

This branch for module8.

## Preparation ##

    1. Rename file "env.file.dummy" and fill needed data.
    2. Replace "host_ip:port" inside "backend/Dockerfile"
    3. Run command "docker-compose up -d --build" for fast build and up.
    4. Check output.txt for security scan results. 
    Run command "docker run -it --net host --pid host --userns host --cap-add audit_control \
                            -e DOCKER_CONTENT_TRUST=$DOCKER_CONTENT_TRUST \
                            -v /etc:/etc:ro \
                            -v /usr/bin/containerd:/usr/bin/containerd:ro \
                            -v /usr/bin/runc:/usr/bin/runc:ro \
                            -v /usr/lib/systemd:/usr/lib/systemd:ro \
                            -v /var/lib:/var/lib:ro \
                            -v /var/run/docker.sock:/var/run/docker.sock:ro \
                            --label docker_bench_security \
                            docker/docker-bench-security"
    5. Run "dive backend:latest" for scan.
    6. Run "dive frontend:latest" for scan.

Frontend dive scan result.
![Frontend scan](frontend.png)
Backend dive scan result.
![Backend scan](backend.png)
