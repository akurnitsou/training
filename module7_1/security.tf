### --- === Security groups === --- ###

resource "aws_security_group" "okurnutsov_external_ssh_access_module71" {
  name          = "ssh-access-from-minsk-module71"
  description   = "Allow SSH traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc_module71.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["104.59.125.84/32", "216.70.7.11/32", "80.94.174.82/32", "82.209.242.80/29", "86.57.155.180/32", "86.57.158.250/32"]
  }
  egress {
    from_port   = 0 #Allow all traffic
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-external-ssh-access-module71"
                  ))
}

resource "aws_security_group" "okurnutsov_internal_ssh_access_module71" {
  name          = "ssh-access-module71"
  description   = "Allow SSH traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc_module71.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_vpc_module71.cidr_block]
  }
  egress {
    from_port   = 0 #Allow all traffic
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-internal-ssh-access-module71"
                  ))
}

resource "aws_security_group" "okurnutsov_web_access_module71" {
  name          = "okurnitsov-web-access-module71"
  description   = "Allow web traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc_module71.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0 
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-web-access-module71"
                  ))
}

resource "aws_security_group" "okurnutsov_icmp_access_module71" {
  name          = "okurnitsov-icmp-access-module71"
  description   = "Allow icmp traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc_module71.id

  ingress {
    from_port   = "-1"
    to_port     = "-1"
    protocol    = "icmp"
    cidr_blocks = [aws_vpc.okurnitsov_vpc_module71.cidr_block]
  }
  egress {
    from_port   = "-1"
    to_port     = "-1"
    protocol    = "icmp"
    cidr_blocks = [aws_vpc.okurnitsov_vpc_module71.cidr_block]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-icmp-access-module71"
                  ))
}

resource "aws_security_group" "okurnutsov_tomcat_access_module71" {
  name          = "tomcat-access-module71"
  description   = "Allow tomcat traffic"
  vpc_id        = aws_vpc.okurnitsov_vpc_module71.id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_vpc_module71.cidr_block]
  }
  ingress {
    from_port   = 8009
    to_port     = 8009
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_vpc_module71.cidr_block]
  }
  egress {
    from_port   = 0 
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-tomcat-access-module71"
                  ))
}
