variable "access_key_id" {
}

variable "secret_access_key" {
}

variable "ssh_key" {
}

variable "region" {
  default  = "us-east-1"
}

variable "assume_role" {
  default = "arn:aws:iam::242906888793:role/AWS_Sandbox"
}

variable "session_name" {
  default = "AWS_Sandbox"
}

variable "vpc_network" {
  default = "172.16.0.0/16"
}

variable "public_subnets" {
  default = ["172.16.0.0/24", "172.16.1.0/24"]
}

variable "private_subnets" {
  default = ["172.16.2.0/24", "172.16.3.0/24"]
}

variable "common_tags" {
  type = map(string)
  default = {
    project = "devops-training"
    client = "Coherent"    
    owner = "olegkurnitsov@coherentsolutions.com"
  }
}