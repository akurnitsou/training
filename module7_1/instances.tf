### --- === IAM for EC2 === --- ###

resource "aws_iam_role" "okurnitsov_allow_ec2_access_to_route53_module71" {
  name = "okurnitsov_allow_ec2_access_to_route53_module71"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-allow-ec2-access-to-route53-module71"
                              )) 
} 

resource "aws_iam_role_policy_attachment" "okurnitsov_attach_route53_policy" {
    role = aws_iam_role.okurnitsov_allow_ec2_access_to_route53_module71.name
    policy_arn = "arn:aws:iam::aws:policy/AmazonRoute53DomainsFullAccess"
}

resource "aws_iam_instance_profile" "okurnitsov_instance_profile_for_ec2" {
  name = "okurnitsov_instance_profile_for_ec2"
  role = aws_iam_role.okurnitsov_allow_ec2_access_to_route53_module71.name
}

### --- === EC2 instances === --- ###

resource "aws_key_pair" "okurnitsov_key_pair_module71" {
  key_name                    = "okurnitsov-key-pair-module71"
  public_key                  = var.ssh_key
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-key-pair-module71"
                              )) 
}

### --- === NAT instances === --- ###

resource "aws_instance" "okurnitsov_nat_instance_module71" {
  ami                         = "ami-00a9d4a05375b2763"
  instance_type               = "t2.nano"
  subnet_id                   = aws_subnet.okurnitsov_public_subnets_module71[0].id
  vpc_security_group_ids      = [aws_security_group.okurnutsov_web_access_module71.id, aws_security_group.okurnutsov_internal_ssh_access_module71.id, aws_security_group.okurnutsov_icmp_access_module71.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair_module71.id
  depends_on                  = [aws_security_group.okurnutsov_external_ssh_access_module71]
  source_dest_check           = "false"
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-nat-instance-module71"
                              )) 
}

### --- === BASTION instances === --- ###

resource "aws_instance" "okurnitsov_bastion_instance_module71" {
  ami                         = "ami-0817d428a6fb68645"
  instance_type               = "t2.nano"
  subnet_id                   = aws_subnet.okurnitsov_public_subnets_module71[0].id
  vpc_security_group_ids      = [aws_security_group.okurnutsov_external_ssh_access_module71.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair_module71.id
  # lifecycle {
  #   prevent_destroy           = "true"
  # }
  depends_on                  = [aws_security_group.okurnutsov_external_ssh_access_module71]
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-bastion-instance-module71"
                              )) 
}

### --- === balancer instances === --- ###

resource "aws_instance" "okurnitsov_balancer_instance_module71" {
  ami                         = "ami-0817d428a6fb68645"
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.okurnitsov_public_subnets_module71[0].id
  vpc_security_group_ids      = [aws_security_group.okurnutsov_web_access_module71.id, aws_security_group.okurnutsov_internal_ssh_access_module71.id, aws_security_group.okurnutsov_icmp_access_module71.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair_module71.id
  iam_instance_profile        = aws_iam_instance_profile.okurnitsov_instance_profile_for_ec2.name
  depends_on                  = [aws_instance.okurnitsov_node_instance_module71]
  connection {
      type                    = "ssh"
      agent                   = false
      user                    = "ubuntu"
      host                    = self.private_ip
      private_key             = file("issoft_ssh_key")
      bastion_host            = aws_instance.okurnitsov_bastion_instance_module71.public_ip
      bastion_private_key     = file("issoft_ssh_key")
    }
  provisioner "file" {
    source      = "000-default.conf"
    destination = "/home/ubuntu/000-default.conf"
  }
  provisioner "remote-exec" {
      inline = [
        "sudo apt update",
        "sudo apt upgrade -y",
        "sudo add-apt-repository ppa:certbot/certbot -y",
        "sudo apt install apache2 apache2-bin apache2-utils python-certbot-apache -y",
        "sudo a2enmod proxy",
        "sudo a2enmod proxy_http",
        "sudo a2enmod proxy_balancer",
        "sudo a2enmod lbmethod_byrequests",
        "sudo a2enmod rewrite",
        "sudo a2enmod headers",
        "sudo a2enmod ssl",
        "echo '<VirtualHost *:443>' >> /home/ubuntu/000-default.conf",
        "echo '<Proxy balancer://lbgroup>' >> /home/ubuntu/000-default.conf",
        "echo 'BalancerMember http://${aws_instance.okurnitsov_node_instance_module71[0].private_ip}:8080/testapp' >> /home/ubuntu/000-default.conf",
        "echo 'BalancerMember http://${aws_instance.okurnitsov_node_instance_module71[1].private_ip}:8080/testapp' >> /home/ubuntu/000-default.conf",
        "echo '</Proxy>' >> /home/ubuntu/000-default.conf",
        "echo 'ProxyPreserveHost On' >> /home/ubuntu/000-default.conf",
        "echo 'ProxyPass /testapp balancer://lbgroup' >> /home/ubuntu/000-default.conf",
        "echo 'ProxyPassReverse /testapp balancer://lbgroup' >> /home/ubuntu/000-default.conf",
        "echo '</VirtualHost>' >> /home/ubuntu/000-default.conf",
        "sudo cp /home/ubuntu/000-default.conf /etc/apache2/sites-enabled/000-default.conf",
        "sudo cp /home/ubuntu/000-default.conf /etc/apache2/sites-enabled/balancer.okurnitsov.test.coherentprojects.net.conf",
        "sudo systemctl enable apache2",
        "sudo systemctl start apache2",
        "sudo systemctl restart apache2",
        "sudo certbot certonly --apache --agree-tos --register-unsafely-without-email -d balancer.okurnitsov.test.coherentprojects.net",
        "sudo systemctl restart apache2",
    ]
  }
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-balancer-instance-module71"
                              )) 
}

### --- === node instances === --- ###

resource "aws_instance" "okurnitsov_node_instance_module71" {
  count                       = length(var.public_subnets)
  ami                         = "ami-0817d428a6fb68645"
  instance_type               = "t2.micro"
  subnet_id                   = element(aws_subnet.okurnitsov_private_subnets_module71.*.id, count.index) 
  vpc_security_group_ids      = [aws_security_group.okurnutsov_icmp_access_module71.id, aws_security_group.okurnutsov_internal_ssh_access_module71.id, aws_security_group.okurnutsov_tomcat_access_module71.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair_module71.id
  associate_public_ip_address = "false"
  connection {
      type                    = "ssh"
      agent                   = false
      user                    = "ubuntu"
      host                    = self.private_ip
      private_key             = file("issoft_ssh_key")
      bastion_host            = aws_instance.okurnitsov_bastion_instance_module71.public_ip
      bastion_private_key     = file("issoft_ssh_key")
    }
  provisioner "file" {
    source                    = "tomcat.service"
    destination               = "/home/ubuntu/tomcat.service"
  }
  provisioner "remote-exec" {
      inline = [
        "sudo ufw allow 8080",
        "TOMCAT_IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)",
        "echo tomcat ip:$TOMCAT_IP >> index.html",
        "sudo apt install -y default-jre",
        "wget https://downloads.apache.org/tomcat/tomcat-8/v8.5.59/bin/apache-tomcat-8.5.59.tar.gz",
        "tar -xvzf apache-tomcat-8.5.59.tar.gz",
        "sudo mv apache-tomcat-8.5.59 tomcat",
        "sudo cp -r tomcat /usr/share",
        "sudo mkdir -p /usr/share/tomcat/webapps/testapp",
        "sudo cp index.html /usr/share/tomcat/webapps/testapp/index.html",
        "sudo cp /home/ubuntu/tomcat.service /etc/systemd/system/tomcat.service",
        "sudo systemctl daemon-reload",
        "sudo systemctl start tomcat",
    ]
  }

  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-node-instance-module71-${count.index+1}"
                              )) 
}