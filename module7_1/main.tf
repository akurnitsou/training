### --- === VPC === --- ###

data "aws_availability_zones" "available_zones_in_current_region" {
  state                   = "available"
}

resource "aws_vpc" "okurnitsov_vpc_module71" {
  cidr_block              = var.vpc_network
  enable_dns_hostnames    = true
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-vpc-module71"
                          ))  
}

### --- === Subnets === --- ###

resource "aws_subnet" "okurnitsov_private_subnets_module71" {
  count                   = length(var.private_subnets)
  vpc_id                  = aws_vpc.okurnitsov_vpc_module71.id
  cidr_block              = var.private_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-private-subnets-module71-${count.index+1}"
                          ))
}

resource "aws_subnet" "okurnitsov_public_subnets_module71" {
  count                   = length(var.public_subnets)
  vpc_id                  = aws_vpc.okurnitsov_vpc_module71.id
  cidr_block              = var.public_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-public-subnets-module71-${count.index+1}"
                          ))
}


### --- === Internet gateway === --- ###

resource "aws_internet_gateway" "okurnitsov_igw_module71" {
  vpc_id                  = aws_vpc.okurnitsov_vpc_module71.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-igw-module71"
                          ))
}

### --- === Route tables === --- ###

resource "aws_route_table" "okurnitsov_vpc_public_route_table_module71" {
  vpc_id = aws_vpc.okurnitsov_vpc_module71.id
  route {
    cidr_block            = "0.0.0.0/0"
    gateway_id            = aws_internet_gateway.okurnitsov_igw_module71.id
  }
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-vpc-public-route-table-module71"
                          ))
}

resource "aws_route_table" "okurnitsov_vpc_private_route_table_module71" {
  vpc_id                  = aws_vpc.okurnitsov_vpc_module71.id
  route {
    cidr_block            = "0.0.0.0/0"
    instance_id           = aws_instance.okurnitsov_nat_instance_module71.id
  }
  depends_on              = [aws_instance.okurnitsov_nat_instance_module71]
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-vpc-private-route-table-module71"
                          ))
}

### --- === Route tables association === --- ###

resource "aws_route_table_association" "associate_public_subnets_with_public_route_table1_module71" {
  subnet_id      = aws_subnet.okurnitsov_public_subnets_module71[0].id
  route_table_id = aws_route_table.okurnitsov_vpc_public_route_table_module71.id
}

resource "aws_route_table_association" "associate_public_subnets_with_public_route_table2" {
  subnet_id      = aws_subnet.okurnitsov_public_subnets_module71[1].id
  route_table_id = aws_route_table.okurnitsov_vpc_public_route_table_module71.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table1" {
  subnet_id      = aws_subnet.okurnitsov_private_subnets_module71[0].id
  route_table_id = aws_route_table.okurnitsov_vpc_private_route_table_module71.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table2" {
  subnet_id      = aws_subnet.okurnitsov_private_subnets_module71[1].id
  route_table_id = aws_route_table.okurnitsov_vpc_private_route_table_module71.id
}

# ### --- === Route 53 === --- ###

data "aws_route53_zone" "check_zone_id" {
  name         = "test.coherentprojects.net."
}

resource "aws_route53_zone" "okurnitsov_main_zone" {
  name        = "okurnitsov.test.coherentprojects.net."
}

resource "aws_route53_record" "okurnitsov_main_zone_record" {
  zone_id     = data.aws_route53_zone.check_zone_id.zone_id
  name        = aws_route53_zone.okurnitsov_main_zone.name
  type        = "NS"
  ttl         = "30"
  records     = aws_route53_zone.okurnitsov_main_zone.name_servers
}

resource "aws_route53_record" "balancer_route53_zone" {
  zone_id     = aws_route53_zone.okurnitsov_main_zone.zone_id
  name        = "balancer.${aws_route53_zone.okurnitsov_main_zone.name}"
  type        = "A"
  ttl         = "300"
  records     = [aws_instance.okurnitsov_balancer_instance_module71.public_ip]
}