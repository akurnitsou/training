data "template_file" "okurnitsov_frontend_task_definition_template" {
    template = file("frontend_task_definition.json.tpl")
    vars = {
      REPOSITORY_URL = replace(aws_ecr_repository.okurnitsov_ecs_frontend.repository_url, "https://", "")
    }
}

data "template_file" "okurnitsov_backend_task_definition_template" {
    template = file("backend_task_definition.json.tpl")
    vars = {
      REPOSITORY_URL = replace(aws_ecr_repository.okurnitsov_ecs_backend.repository_url, "https://", "")
    }
}

resource "aws_ecs_cluster" "okurnitsov_ecs_cluster" {
  name                 = "okurnitsov-ecs-cluster"
  tags                 = merge(var.common_tags, map(
                         "Name", "okurnitsov-ecs-cluster"
                       )) 
}

resource "aws_ecs_service" "okurnitsov_frontend_worker" {
  name            = "okurnitsov-frontend-worker"
  cluster         = aws_ecs_cluster.okurnitsov_ecs_cluster.id
  task_definition = aws_ecs_task_definition.okurnitsov_frontend_task_definition.arn
  desired_count   = 1
}

resource "aws_ecs_service" "okurnitsov_backend_worker" {
  name            = "okurnitsov-backend-worker"
  cluster         = aws_ecs_cluster.okurnitsov_ecs_cluster.id
  task_definition = aws_ecs_task_definition.okurnitsov_backend_task_definition.arn
  desired_count   = 1
}

resource "aws_ecs_task_definition" "okurnitsov_frontend_task_definition" {
  family                = "okurnitsov_frontend_worker"
  container_definitions = data.template_file.okurnitsov_frontend_task_definition_template.rendered
  execution_role_arn    = "arn:aws:iam::242906888793:role/okurnitsov-ecs-task"
  cpu = 512
  memory = 512
}

resource "aws_ecs_task_definition" "okurnitsov_backend_task_definition" {
  family                = "okurnitsov_backend_worker"
  container_definitions = data.template_file.okurnitsov_backend_task_definition_template.rendered
  execution_role_arn    = "arn:aws:iam::242906888793:role/okurnitsov-ecs-task"
  cpu = 512
  memory = 512
}

### --- === Autoscaling group === --- ###

data "aws_iam_policy_document" "ecs_agent" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "okurnitsov_ecs_agent" {
  name               = "okurnitsov-ecs-agent"
  assume_role_policy = data.aws_iam_policy_document.ecs_agent.json
}

resource "aws_iam_role_policy_attachment" "okurnitsov_ecs_agent_attachment" {
  role       = aws_iam_role.okurnitsov_ecs_agent.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "okurnitsov_ecs_agent_instance_profile" {
  name = "okurnitsov-ecs-agent"
  role = aws_iam_role.okurnitsov_ecs_agent.name
}

resource "aws_launch_configuration" "okurnitsov_ecs_launch_config" {
    image_id             = data.aws_ami.ecs_optimized.id
    iam_instance_profile = aws_iam_instance_profile.okurnitsov_ecs_agent_instance_profile.name
    security_groups      = [aws_security_group.okurnutsov_web_access.id, aws_security_group.okurnutsov_internal_ssh_access.id, aws_security_group.okurnutsov_icmp_access.id, aws_security_group.okurnutsov_backend_access.id]
    user_data            = "#!/bin/bash\necho ECS_CLUSTER=okurnitsov-ecs-cluster >> /etc/ecs/ecs.config"
    instance_type        = "t2.small"
    key_name             = aws_key_pair.okurnitsov_key_pair.id
}

resource "aws_autoscaling_group" "okurnitsov_ecs_asg" {
    name                      = "okurnitsov-ecs-asg"
    vpc_zone_identifier       = [aws_subnet.okurnitsov_private_subnets[0].id]
    launch_configuration      = aws_launch_configuration.okurnitsov_ecs_launch_config.name
    desired_capacity          = 1
    min_size                  = 1
    max_size                  = 3
    health_check_grace_period = 300
    health_check_type         = "EC2"
    depends_on                = [aws_iam_role.okurnitsov_ecs_agent]
}

data "aws_iam_policy_document" "ecs_task" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "okurnitsov_ecs_task" {
  name               = "okurnitsov-ecs-task"
  assume_role_policy = data.aws_iam_policy_document.ecs_task.json
}

resource "aws_iam_role_policy_attachment" "okurnitsov_ecs_task_attachment_ecs" {
  role       = aws_iam_role.okurnitsov_ecs_task.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "okurnitsov_ecs_task_attachment_ssm" {
  role       = aws_iam_role.okurnitsov_ecs_task.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMReadOnlyAccess"
}

resource "aws_cloudwatch_log_group" "okurnitsov_ecs_frontend" {
  name        = "ecs/okurnitsov-frontend"
  tags                 = merge(var.common_tags, map(
                         "Name", "okurnitsov-ecs-frontend"
                       )) 
}

resource "aws_cloudwatch_log_group" "okurnitsov_ecs_backend" {
  name        = "ecs/okurnitsov-backend"
  tags                 = merge(var.common_tags, map(
                         "Name", "okurnitsov-ecs-backend"
                       )) 
}