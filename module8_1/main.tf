### --- === VPC === --- ###

data "aws_availability_zones" "available_zones_in_current_region" {
  state                   = "available"
}

resource "aws_vpc" "okurnitsov_vpc" {
  cidr_block              = var.vpc_network
  enable_dns_hostnames    = true
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-vpc"
                          ))  
}

### --- === Subnets === --- ###

resource "aws_subnet" "okurnitsov_private_subnets" {
  count                   = length(var.private_subnets)
  vpc_id                  = aws_vpc.okurnitsov_vpc.id
  cidr_block              = var.private_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags = merge(var.common_tags, map(
    "Name", "okurnitsov-private-subnets-${count.index+1}"
  ))
}

resource "aws_subnet" "okurnitsov_public_subnets" {
  count                   = length(var.public_subnets)
  vpc_id                  = aws_vpc.okurnitsov_vpc.id
  cidr_block              = var.public_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-public-subnets-${count.index+1}"
                          ))
}

resource "aws_subnet" "okurnitsov_db_subnets" {
  count                   = length(var.db_subnets)
  vpc_id                  = aws_vpc.okurnitsov_vpc.id
  cidr_block              = var.db_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-db-subnets"
                          ))
}

### --- === Internet gateway === --- ###

resource "aws_internet_gateway" "okurnitsov_igw" {
  vpc_id                  = aws_vpc.okurnitsov_vpc.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-igw"
                          ))
}

### --- === Route tables === --- ###

resource "aws_route_table" "okurnitsov_vpc_public_route_table" {
  vpc_id = aws_vpc.okurnitsov_vpc.id
  route {
    cidr_block            = "0.0.0.0/0"
    gateway_id            = aws_internet_gateway.okurnitsov_igw.id
  }
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-vpc-public-route-table"
                          ))
}

resource "aws_route_table" "okurnitsov_vpc_private_route_table" {
  vpc_id                  = aws_vpc.okurnitsov_vpc.id
  route {
    cidr_block            = "0.0.0.0/0"
    instance_id           = aws_instance.okurnitsov_nat_instance.id
  }
  depends_on              = [aws_instance.okurnitsov_nat_instance]
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-vpc-private-route-table"
                          ))
}

### --- === Route tables association === --- ###

resource "aws_route_table_association" "associate_public_subnets_with_public_route_table1" {
  subnet_id               = aws_subnet.okurnitsov_public_subnets[0].id
  route_table_id          = aws_route_table.okurnitsov_vpc_public_route_table.id
}

resource "aws_route_table_association" "associate_public_subnets_with_public_route_table2" {
  subnet_id               = aws_subnet.okurnitsov_public_subnets[1].id
  route_table_id          = aws_route_table.okurnitsov_vpc_public_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table1" {
  subnet_id               = aws_subnet.okurnitsov_private_subnets[0].id
  route_table_id          = aws_route_table.okurnitsov_vpc_private_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table2" {
  subnet_id               = aws_subnet.okurnitsov_private_subnets[1].id
  route_table_id          = aws_route_table.okurnitsov_vpc_private_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table3" {
  subnet_id               = aws_subnet.okurnitsov_db_subnets[0].id
  route_table_id          = aws_route_table.okurnitsov_vpc_private_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table4" {
  subnet_id               = aws_subnet.okurnitsov_db_subnets[1].id
  route_table_id          = aws_route_table.okurnitsov_vpc_private_route_table.id
}

### --- === Route 53 === --- ###

data "aws_route53_zone" "check_zone_id" {
  name         = "test.coherentprojects.net."
}

resource "aws_route53_zone" "okurnitsov_main_zone" {
  name        = "okurnitsov.test.coherentprojects.net."
}

resource "aws_route53_record" "okurnitsov_main_zone_record" {
  zone_id     = data.aws_route53_zone.check_zone_id.zone_id
  name        = aws_route53_zone.okurnitsov_main_zone.name
  type        = "NS"
  ttl         = "30"
  records     = aws_route53_zone.okurnitsov_main_zone.name_servers
}

resource "aws_route53_record" "balancer_route53_zone" {
  zone_id     = aws_route53_zone.okurnitsov_main_zone.zone_id
  name        = aws_route53_zone.okurnitsov_main_zone.name
  type        = "A"
  alias {
      name = aws_lb.okurnitsov_alb.dns_name
      zone_id = aws_lb.okurnitsov_alb.zone_id
      evaluate_target_health = true
  }
  depends_on  = [aws_lb.okurnitsov_alb]
}

resource "aws_route53_record" "balancer_route53_zone_backend" {
  zone_id     = aws_route53_zone.okurnitsov_main_zone.zone_id
  name        = "backend.${aws_route53_zone.okurnitsov_main_zone.name}"
  type        = "A"
  alias {
      name = aws_lb.okurnitsov_alb.dns_name
      zone_id = aws_lb.okurnitsov_alb.zone_id
      evaluate_target_health = true
  }
  depends_on  = [aws_lb.okurnitsov_alb]
}

### --- === ALB === --- ###

resource "aws_lb" "okurnitsov_alb" {
  name               = "okurnitsov-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.okurnutsov_web_access.id]
  subnets            = aws_subnet.okurnitsov_public_subnets.*.id
  depends_on  = [aws_launch_configuration.okurnitsov_ecs_launch_config]
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-alb"
                          ))
}

resource "aws_lb_target_group" "okurnitsov_alb_target_group" {
  name     = "okurnitsov-alb-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.okurnitsov_vpc.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-alb-target-group"
                          ))
}

resource "aws_lb_target_group_attachment" "okurnitsov_alb_target_group_attachment" {
  target_group_arn = aws_lb_target_group.okurnitsov_alb_target_group.arn
  target_id        = data.aws_instance.ecs_instance.id
  port             = 80
}

resource "aws_lb_listener" "okurnitsov_alb_listener_redirect" {
  load_balancer_arn = aws_lb.okurnitsov_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "okurnitsov_alb_listener" {
  load_balancer_arn = aws_lb.okurnitsov_alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  certificate_arn   = "arn:aws:acm:us-east-1:242906888793:certificate/f2e8b0e6-aa7a-48be-bedb-945df54a55f2"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.okurnitsov_alb_target_group.arn
  }
}

resource "aws_lb_listener_rule" "okurnitsov_alb_listener_backend" {
  listener_arn = aws_lb_listener.okurnitsov_alb_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.okurnitsov_alb_target_group_backend.arn
  }

  condition {
    host_header {
      values = ["backend.*"]
    }
  }
}

resource "aws_lb_target_group" "okurnitsov_alb_target_group_backend" {
  name     = "okurnitsov-alb-target-group-back"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.okurnitsov_vpc.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-alb-target-backend"
                          ))
}

resource "aws_lb_target_group_attachment" "okurnitsov_alb_target_group_attachment_backend" {
  target_group_arn = aws_lb_target_group.okurnitsov_alb_target_group_backend.arn
  target_id        = data.aws_instance.ecs_instance.id
  port             = 8080
}

data "aws_instance" "ecs_instance" {
  depends_on  = [aws_launch_configuration.okurnitsov_ecs_launch_config]
  filter {
    name   = "image-id"
    values = [data.aws_ami.ecs_optimized.id]
  }
  
}