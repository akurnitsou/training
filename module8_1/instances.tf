### --- === EC2 instances === --- ###

data "aws_ami" "ecs_optimized" {
  most_recent = true
  filter {
    name                  = "virtualization-type"
    values                = ["hvm"]
  }
  filter {
    name                  = "name"
    values                = ["amzn2-ami-ecs-hvm-2*"]
  }
  owners                  = ["amazon"] 
}

data "aws_ami" "nat_instance" {
  most_recent = true
  filter {
    name                  = "virtualization-type"
    values                = ["hvm"]
  }
  filter {
    name                  = "name"
    values                = ["amzn-ami-vpc-nat-hvm*"]
  }
  owners                  = ["amazon"]
}

data "aws_ami" "bastion_instance" {
  most_recent = true
  filter {
    name                  = "virtualization-type"
    values                = ["hvm"]
  }
  filter {
    name                  = "name"
    values                = ["amzn2-ami-hvm-2*"]
  }
  owners                  = ["amazon"] 
}

resource "aws_key_pair" "okurnitsov_key_pair" {
  key_name                    = "okurnitsov-key-pair"
  public_key                  = var.ssh_key
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-nat-instance"
                              )) 
}

### --- === NAT instances === --- ###

resource "aws_instance" "okurnitsov_nat_instance" {
  ami                         = data.aws_ami.nat_instance.id
  instance_type               = "t2.nano"
  subnet_id                   = aws_subnet.okurnitsov_public_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnutsov_web_access.id, aws_security_group.okurnutsov_internal_ssh_access.id, aws_security_group.okurnutsov_icmp_access.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair.id
  depends_on                  = [aws_security_group.okurnutsov_external_ssh_access]
  source_dest_check           = "false"
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-nat-instance"
                              )) 
}

### --- === BASTION instances === --- ###

resource "aws_instance" "okurnitsov_bastion_instance" {
  ami                         = data.aws_ami.bastion_instance.id
  instance_type               = "t2.nano"
  subnet_id                   = aws_subnet.okurnitsov_public_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnutsov_external_ssh_access.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair.id
  # lifecycle {
  #   prevent_destroy           = "true"
  # }
  depends_on                  = [aws_security_group.okurnutsov_external_ssh_access]
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-bastion-instance"
                              )) 
}