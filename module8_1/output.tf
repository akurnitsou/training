output "bastion_host_ip_address" {
  value       = aws_instance.okurnitsov_bastion_instance.public_ip
  description = "The ip address of the bastion host"
}

output "db_endpoint" {
  value       = aws_rds_cluster.okurnitsov_rds_cluster.endpoint
  description = "The ip address of the bastion host"
}