[
  {
    "name": "okurnitsov_backend",
    "image": "${REPOSITORY_URL}:latest",
    "cpu": 512,
    "memory": 512,
    "essential": true,
    "environment": [
      {
        "name": "DB_USERNAME",
        "value": "db_user"
      },  
      {
        "name": "DB_PASSWORD",
        "value": "db_password"
      },  
      {
        "name": "DB_URL",
        "value": "okurnitsov-rds.cluster-cqnuqytrqlro.us-east-1.rds.amazonaws.com"
      },  
      {
        "name": "DB_PORT",
        "value": "3306"
      },  
      {
        "name": "DB_NAME",
        "value": "realworld"
      }  
    ],
    "portMappings": [
       {
         "containerPort": 8080,
         "hostPort": 8080,
         "protocol": "tcp"
       }
     ],
     "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-group": "ecs/okurnitsov-backend",
            "awslogs-region": "us-east-1"
            }
     }      
  }
]