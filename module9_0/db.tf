resource "aws_db_subnet_group" "okurnitsov_environment_rds_db_subnet" {
  name                        = "okurnitsov-environment-rds-db-subnet-group"
  subnet_ids                  = aws_subnet.okurnitsov_environment_db_subnets.*.id
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-environment-rds-db-subnet-group"
                              )) 
}

resource "aws_rds_cluster" "okurnitsov_environment_rds_cluster" {
  cluster_identifier          = "okurnitsov-environment-rds"
  engine                      = "aurora-postgresql"
  engine_version              = "11.6"
  database_name               = var.db_name
  master_username             = var.db_user
  master_password             = var.db_pass
  db_subnet_group_name        = aws_db_subnet_group.okurnitsov_environment_rds_db_subnet.name
  vpc_security_group_ids      = [aws_security_group.okurnitsov_environment_db_access.id]
  backup_retention_period     = 5
  preferred_backup_window     = "07:00-09:00"
  skip_final_snapshot         = true
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-environment-rds-db"
                              )) 
}

resource "aws_rds_cluster_instance" "okurnitsov_environment_rds_db_instance" {
  cluster_identifier          = aws_rds_cluster.okurnitsov_environment_rds_cluster.id
  identifier                  = "okurnitsov-environment-rds-db-instance"
  instance_class              = "db.t3.medium"
  engine                      = "aurora-postgresql"
  engine_version              = "11.6"
  db_subnet_group_name        = aws_db_subnet_group.okurnitsov_environment_rds_db_subnet.name
  apply_immediately           = true
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-environment-rds-db-instance"
                              ))   
}

resource "aws_rds_cluster" "okurnitsov_rds_cluster" {
  cluster_identifier          = "okurnitsov-rds"
  engine                      = "aurora-mysql"
  engine_version              = "5.7.mysql_aurora.2.07.2"
  database_name               = var.db_name_env
  master_username             = var.db_user_env
  master_password             = var.db_pass_env
  db_subnet_group_name        = aws_db_subnet_group.okurnitsov_environment_rds_db_subnet.name
  vpc_security_group_ids      = [aws_security_group.okurnitsov_db_access.id]
  backup_retention_period     = 5
  preferred_backup_window     = "07:00-09:00"
  skip_final_snapshot         = true
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-rds-db"
                              )) 
}

resource "aws_rds_cluster_instance" "okurnitsov_rds_db_instance" {
  cluster_identifier          = aws_rds_cluster.okurnitsov_rds_cluster.id
  identifier                  = "okurnitsov-rds-db-instance"
  instance_class              = "db.t3.small"
  engine                      = "aurora-mysql"
  engine_version              = "5.7.mysql_aurora.2.07.2"
  db_subnet_group_name        = aws_db_subnet_group.okurnitsov_environment_rds_db_subnet.name
  apply_immediately           = true
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-rds-db-instance"
                              ))   
}