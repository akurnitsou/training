[
  {
    "name": "okurnitsov_frontend",  
    "image": "${REPOSITORY_URL}:develop-latest",  
    "cpu": 512,
    "memory": 512,
    "portMappings": [
       {
         "containerPort": 80,
         "hostPort": 80,
         "protocol": "tcp"
       }
     ],
     "essential": true,
     "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-group": "ecs/okurnitsov-frontend",
            "awslogs-region": "us-east-1"
            }
     }     
  }
]