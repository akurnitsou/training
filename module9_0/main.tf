### --- === VPC === --- ###

data "aws_availability_zones" "available_zones_in_current_region" {
  state                   = "available"
}

resource "aws_vpc" "okurnitsov_environment_vpc" {
  cidr_block              = var.vpc_network
  enable_dns_hostnames    = true
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-environment-vpc"
                          ))  
}

### --- === Subnets === --- ###

resource "aws_subnet" "okurnitsov_environment_private_subnets" {
  count                   = length(var.private_subnets)
  vpc_id                  = aws_vpc.okurnitsov_environment_vpc.id
  cidr_block              = var.private_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags = merge(var.common_tags, map(
    "Name", "okurnitsov-environment-private-subnets-${count.index+1}"
  ))
}

resource "aws_subnet" "okurnitsov_environment_public_subnets" {
  count                   = length(var.public_subnets)
  vpc_id                  = aws_vpc.okurnitsov_environment_vpc.id
  cidr_block              = var.public_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-environment-public-subnets-${count.index+1}"
                          ))
}

resource "aws_subnet" "okurnitsov_environment_db_subnets" {
  count                   = length(var.db_subnets)
  vpc_id                  = aws_vpc.okurnitsov_environment_vpc.id
  cidr_block              = var.db_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-environment-db-subnets"
                          ))
}

### --- === Internet gateway === --- ###

resource "aws_internet_gateway" "okurnitsov_environment_igw" {
  vpc_id                  = aws_vpc.okurnitsov_environment_vpc.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-environment-igw"
                          ))
}

### --- === Route tables === --- ###

resource "aws_route_table" "okurnitsov_environment_vpc_public_route_table" {
  vpc_id = aws_vpc.okurnitsov_environment_vpc.id
  route {
    cidr_block            = "0.0.0.0/0"
    gateway_id            = aws_internet_gateway.okurnitsov_environment_igw.id
  }
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-environment-vpc-public-route-table"
                          ))
}

resource "aws_route_table" "okurnitsov_environment_vpc_private_route_table" {
  vpc_id                  = aws_vpc.okurnitsov_environment_vpc.id
  route {
    cidr_block            = "0.0.0.0/0"
    instance_id           = aws_instance.okurnitsov_environment_nat_instance.id
  }
  depends_on              = [aws_instance.okurnitsov_environment_nat_instance]
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-environment-vpc-private-route-table"
                          ))
}

### --- === Route tables association === --- ###

resource "aws_route_table_association" "associate_public_subnets_with_public_route_table1" {
  subnet_id               = aws_subnet.okurnitsov_environment_public_subnets[0].id
  route_table_id          = aws_route_table.okurnitsov_environment_vpc_public_route_table.id
}

resource "aws_route_table_association" "associate_public_subnets_with_public_route_table2" {
  subnet_id               = aws_subnet.okurnitsov_environment_public_subnets[1].id
  route_table_id          = aws_route_table.okurnitsov_environment_vpc_public_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table1" {
  subnet_id               = aws_subnet.okurnitsov_environment_private_subnets[0].id
  route_table_id          = aws_route_table.okurnitsov_environment_vpc_private_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table2" {
  subnet_id               = aws_subnet.okurnitsov_environment_private_subnets[1].id
  route_table_id          = aws_route_table.okurnitsov_environment_vpc_private_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table3" {
  subnet_id               = aws_subnet.okurnitsov_environment_db_subnets[0].id
  route_table_id          = aws_route_table.okurnitsov_environment_vpc_private_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table4" {
  subnet_id               = aws_subnet.okurnitsov_environment_db_subnets[1].id
  route_table_id          = aws_route_table.okurnitsov_environment_vpc_private_route_table.id
}

### --- === Route 53 === --- ###

data "aws_route53_zone" "check_zone_id" {
  name         = "test.coherentprojects.net."
}

resource "aws_route53_zone" "okurnitsov_environment_main_zone" {
  name        = "okurnitsov.test.coherentprojects.net."
}

resource "aws_route53_record" "okurnitsov_environment_main_zone_record" {
  zone_id     = data.aws_route53_zone.check_zone_id.zone_id
  name        = aws_route53_zone.okurnitsov_environment_main_zone.name
  type        = "NS"
  ttl         = "30"
  records     = aws_route53_zone.okurnitsov_environment_main_zone.name_servers
}

resource "aws_route53_record" "balancer_route53_zone" {
  zone_id     = aws_route53_zone.okurnitsov_environment_main_zone.zone_id
  name        = aws_route53_zone.okurnitsov_environment_main_zone.name
  type        = "A"
  alias {
      name = aws_lb.okurnitsov_environment_alb.dns_name
      zone_id = aws_lb.okurnitsov_environment_alb.zone_id
      evaluate_target_health = true
  }
  depends_on  = [aws_lb.okurnitsov_environment_alb]
}

resource "aws_route53_record" "balancer_route53_zone_backend" {
  zone_id     = aws_route53_zone.okurnitsov_environment_main_zone.zone_id
  name        = "backend.${aws_route53_zone.okurnitsov_environment_main_zone.name}"
  type        = "A"
  alias {
      name = aws_lb.okurnitsov_environment_alb.dns_name
      zone_id = aws_lb.okurnitsov_environment_alb.zone_id
      evaluate_target_health = true
  }
  depends_on  = [aws_lb.okurnitsov_environment_alb]
}

resource "aws_route53_record" "balancer_route53_zone_frontend" {
  zone_id     = aws_route53_zone.okurnitsov_environment_main_zone.zone_id
  name        = "frontend.${aws_route53_zone.okurnitsov_environment_main_zone.name}"
  type        = "A"
  alias {
      name = aws_lb.okurnitsov_environment_alb.dns_name
      zone_id = aws_lb.okurnitsov_environment_alb.zone_id
      evaluate_target_health = true
  }
  depends_on  = [aws_lb.okurnitsov_environment_alb]
}

resource "aws_route53_record" "balancer_route53_zone_jenkins" {
  zone_id     = aws_route53_zone.okurnitsov_environment_main_zone.zone_id
  name        = "jenkins.${aws_route53_zone.okurnitsov_environment_main_zone.name}"
  type        = "A"
  alias {
      name = aws_lb.okurnitsov_environment_alb.dns_name
      zone_id = aws_lb.okurnitsov_environment_alb.zone_id
      evaluate_target_health = true
  }
  depends_on  = [aws_lb.okurnitsov_environment_alb]
}

resource "aws_route53_record" "balancer_route53_zone_nexus" {
  zone_id     = data.aws_route53_zone.check_zone_id.zone_id
  name        = "nexus-okurnitsov.${data.aws_route53_zone.check_zone_id.name}"
  type        = "A"
  alias {
      name = aws_lb.okurnitsov_environment_alb.dns_name
      zone_id = aws_lb.okurnitsov_environment_alb.zone_id
      evaluate_target_health = true
  }
  depends_on  = [aws_lb.okurnitsov_environment_alb]
}

resource "aws_route53_record" "balancer_route53_zone_sonarqube" {
  zone_id     = data.aws_route53_zone.check_zone_id.zone_id
  name        = "sonarqube-okurnitsov.${data.aws_route53_zone.check_zone_id.name}"
  type        = "A"
  alias {
      name = aws_lb.okurnitsov_environment_alb.dns_name
      zone_id = aws_lb.okurnitsov_environment_alb.zone_id
      evaluate_target_health = true
  }
  depends_on  = [aws_lb.okurnitsov_environment_alb]
}

### --- === ALB === --- ###

resource "aws_lb" "okurnitsov_environment_alb" {
  name               = "okurnitsov-environment-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.okurnitsov_environment_web_access.id]
  subnets            = aws_subnet.okurnitsov_environment_public_subnets.*.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-environment-alb"
                          ))
}

resource "aws_lb_listener" "okurnitsov_environment_alb_listener_redirect" {
  load_balancer_arn = aws_lb.okurnitsov_environment_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "okurnitsov_environment_alb_listener" {
  load_balancer_arn = aws_lb.okurnitsov_environment_alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  certificate_arn   = "arn:aws:acm:us-east-1:242906888793:certificate/f2e8b0e6-aa7a-48be-bedb-945df54a55f2"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Fixed response content"
      status_code  = "503"
    }
  }
}

resource "aws_lb_listener_rule" "okurnitsov_environment_alb_listener_sonarqube" {
  listener_arn = aws_lb_listener.okurnitsov_environment_alb_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.okurnitsov_environment_alb_target_group_sonarqube.arn
  }

  condition {
    host_header {
      values = ["sonarqube-okurnitsov.*"]
    }
  }
}

resource "aws_lb_target_group" "okurnitsov_environment_alb_target_group_sonarqube" {
  name     = "okurnitsov-env-alb-tg-sonarqube"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.okurnitsov_environment_vpc.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-environment-alb-target-sonarqube"
                          ))
}

resource "aws_lb_target_group_attachment" "okurnitsov_environment_alb_target_group_attachment_sonarqube" {
  target_group_arn = aws_lb_target_group.okurnitsov_environment_alb_target_group_sonarqube.arn
  target_id        = aws_instance.okurnitsov_environment_sonar_instance.id
  port             = 9000
}


resource "aws_lb_listener_rule" "okurnitsov_environment_alb_listener_nexus" {
  listener_arn = aws_lb_listener.okurnitsov_environment_alb_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.okurnitsov_environment_alb_target_group_nexus.arn
  }

  condition {
    host_header {
      values = ["nexus-okurnitsov.*"]
    }
  }
}

resource "aws_lb_target_group" "okurnitsov_environment_alb_target_group_nexus" {
  name     = "okurnitsov-env-alb-tg-nexus"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.okurnitsov_environment_vpc.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-environment-alb-target-nexus"
                          ))
}

resource "aws_lb_target_group_attachment" "okurnitsov_environment_alb_target_group_attachment_nexus" {
  target_group_arn = aws_lb_target_group.okurnitsov_environment_alb_target_group_nexus.arn
  target_id        = aws_instance.okurnitsov_environment_nexus_instance.id
  port             = 8081
}

resource "aws_lb_listener_rule" "okurnitsov_environment_alb_listener_jenkins" {
  listener_arn = aws_lb_listener.okurnitsov_environment_alb_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.okurnitsov_environment_alb_target_group_jenkins.arn
  }

  condition {
    host_header {
      values = ["jenkins.*"]
    }
  }
}

resource "aws_lb_target_group" "okurnitsov_environment_alb_target_group_jenkins" {
  name     = "okurnitsov-env-alb-tg-jenkins"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.okurnitsov_environment_vpc.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-environment-alb-target-jenkins"
                          ))
}

resource "aws_lb_target_group_attachment" "okurnitsov_environment_alb_target_group_attachment_jenkins" {
  target_group_arn = aws_lb_target_group.okurnitsov_environment_alb_target_group_jenkins.arn
  target_id        = aws_instance.okurnitsov_environment_jenkins_instance.id
  port             = 8080
}

resource "aws_lb_listener_rule" "okurnitsov_alb_listener_backend" {
  listener_arn = aws_lb_listener.okurnitsov_environment_alb_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.okurnitsov_alb_tg_backend.arn
  }

  condition {
    host_header {
      values = ["backend.*"]
    }
  }
}

resource "aws_lb_target_group" "okurnitsov_alb_tg_backend" {
  name     = "okurnitsov-alb-tg-back"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.okurnitsov_environment_vpc.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-alb-target-backend"
                          ))
}

resource "aws_lb_target_group_attachment" "okurnitsov_alb_target_group_attachment_backend" {
  target_group_arn = aws_lb_target_group.okurnitsov_alb_tg_backend.arn
  target_id        = data.aws_instance.ecs_instance.id
  port             = 8080
}

resource "aws_lb_listener_rule" "okurnitsov_alb_listener_frontend" {
  listener_arn = aws_lb_listener.okurnitsov_environment_alb_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.okurnitsov_alb_tg_frontend.arn
  }

  condition {
    host_header {
      values = ["frontend.*"]
    }
  }
}

resource "aws_lb_target_group" "okurnitsov_alb_tg_frontend" {
  name     = "okurnitsov-alb-tg-front"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.okurnitsov_environment_vpc.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-alb-target-frontend"
                          ))
}

resource "aws_lb_target_group_attachment" "okurnitsov_alb_target_group_attachment_frontend" {
  target_group_arn = aws_lb_target_group.okurnitsov_alb_tg_frontend.arn
  target_id        = data.aws_instance.ecs_instance.id
  port             = 80
}

data "aws_instance" "ecs_instance" {
  depends_on  = [aws_launch_configuration.okurnitsov_ecs_launch_config]
  filter {
    name   = "image-id"
    values = [data.aws_ami.ecs_optimized.id]
  }
}