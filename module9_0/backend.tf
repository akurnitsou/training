### --- === backend === --- ###

terraform {
  backend "s3" {
    bucket         = "okurnitsov-training-terraform-state"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "okurnitsov-training-terraform-locks"
    encrypt        = true
    role_arn       = "arn:aws:iam::242906888793:role/AWS_Sandbox"
    session_name   = "AWS_Sandbox"
  }
}

### --- === S3 for backend === --- ###

resource "aws_s3_bucket" "okurnitsov_training_terraform_state" {
  bucket = "okurnitsov-training-terraform-state"
  versioning {
    enabled = true
  }
  lifecycle_rule {
    enabled = true
    transition {
      days          = 30
      storage_class = "STANDARD_IA" # or "ONEZONE_IA"
    }
    expiration {
      days = 90
    }

  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  tags = merge(var.common_tags, map(
    "Name", "okurnitsov-s3"
  ))
}

### --- === Dinamo DB for backend === --- ###

resource "aws_dynamodb_table" "okurnitsov_training_terraform_locks" {
  name         = "okurnitsov-training-terraform-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = merge(var.common_tags, map(
    "Name", "okurnitsov-dinamo"
  ))
}