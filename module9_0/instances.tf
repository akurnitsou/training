### --- === EC2 instances === --- ###

data "aws_ami" "nat_instance" {
  most_recent = true
  filter {
    name                  = "virtualization-type"
    values                = ["hvm"]
  }
  filter {
    name                  = "name"
    values                = ["amzn-ami-vpc-nat-hvm*"]
  }
  owners                  = ["amazon"]
}

data "aws_ami" "env_instance" {
  most_recent = true
  filter {
    name                  = "virtualization-type"
    values                = ["hvm"]
  }
  filter {
    name                  = "name"
    values                = ["amzn2-ami-hvm-2*"]
  }
  owners                  = ["amazon"] 
}

data "aws_ami" "ecs_optimized" {
  most_recent = true
  filter {
    name                  = "virtualization-type"
    values                = ["hvm"]
  }
  filter {
    name                  = "architecture"
    values                = ["x86_64"]
  }
  filter {
    name                  = "name"
    values                = ["amzn2-ami-ecs-hvm-2*"]
  }
  owners                  = ["amazon"] 
}

resource "aws_key_pair" "okurnitsov_environment_key_pair" {
  key_name                    = "okurnitsov-environment-key-pair"
  public_key                  = var.ssh_key
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-environment-nat-instance"
                              )) 
}

### --- === NAT instances === --- ###

resource "aws_instance" "okurnitsov_environment_nat_instance" {
  ami                         = data.aws_ami.nat_instance.id
  instance_type               = "t2.nano"
  subnet_id                   = aws_subnet.okurnitsov_environment_public_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnitsov_environment_web_access.id, aws_security_group.okurnitsov_environment_internal_ssh_access.id, aws_security_group.okurnitsov_environment_icmp_access.id]
  key_name                    = aws_key_pair.okurnitsov_environment_key_pair.id
  depends_on                  = [aws_security_group.okurnitsov_environment_external_ssh_access]
  source_dest_check           = "false"
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-environment-nat-instance"
                              )) 
}

### --- === BASTION instances === --- ###

resource "aws_instance" "okurnitsov_environment_bastion_instance" {
  ami                         = data.aws_ami.env_instance.id
  instance_type               = "t2.nano"
  subnet_id                   = aws_subnet.okurnitsov_environment_public_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnitsov_environment_external_ssh_access.id]
  key_name                    = aws_key_pair.okurnitsov_environment_key_pair.id
  lifecycle {
    prevent_destroy           = "true"
  }
  depends_on                  = [aws_security_group.okurnitsov_environment_external_ssh_access]
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-environment-bastion-instance"
                              )) 
}

### --- === Jenkins instances === --- ###

resource "aws_instance" "okurnitsov_environment_jenkins_instance" {
  ami                         = data.aws_ami.env_instance.id
  instance_type               = "t3a.small"
  subnet_id                   = aws_subnet.okurnitsov_environment_private_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnitsov_environment_internal_ssh_access.id, aws_security_group.okurnitsov_environment_jenkins_access.id, aws_security_group.okurnitsov_environment_web_access.id]
  key_name                    = aws_key_pair.okurnitsov_environment_key_pair.id
  lifecycle {
    prevent_destroy           = "true"
  }
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-environment-jenkins-instance"
                              )) 
}

### --- === Nexus instances === --- ###

resource "aws_instance" "okurnitsov_environment_nexus_instance" {
  ami                         = data.aws_ami.env_instance.id
  instance_type               = "t3a.medium"
  subnet_id                   = aws_subnet.okurnitsov_environment_private_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnitsov_environment_internal_ssh_access.id, aws_security_group.okurnitsov_environment_nexus_access.id, aws_security_group.okurnitsov_environment_web_access.id]
  key_name                    = aws_key_pair.okurnitsov_environment_key_pair.id
  lifecycle {
    prevent_destroy           = "true"
  }
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-environment-nexus-instance"
                              )) 
}

### --- === Sonar instances === --- ###

resource "aws_instance" "okurnitsov_environment_sonar_instance" {
  ami                         = data.aws_ami.env_instance.id
  instance_type               = "t3a.small"
  subnet_id                   = aws_subnet.okurnitsov_environment_private_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnitsov_environment_internal_ssh_access.id, aws_security_group.okurnitsov_environment_sonarqube_access.id, aws_security_group.okurnitsov_environment_db_access.id, aws_security_group.okurnitsov_environment_web_access.id]
  key_name                    = aws_key_pair.okurnitsov_environment_key_pair.id
  lifecycle {
    prevent_destroy           = "true"
  }
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-environment-sonar-instance"
                              )) 
}

### --- === empty instances === --- ###

resource "aws_instance" "okurnitsov_environment_empty_instance" {
  count                       = 5
  ami                         = data.aws_ami.env_instance.id
  instance_type               = "t3a.small"
  subnet_id                   = aws_subnet.okurnitsov_environment_private_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnitsov_environment_icmp_access.id, aws_security_group.okurnitsov_environment_internal_ssh_access.id, aws_security_group.okurnitsov_environment_jenkins_access.id, aws_security_group.okurnitsov_environment_web_access.id]
  key_name                    = aws_key_pair.okurnitsov_environment_key_pair.id
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-environment-instance-${count.index+1}"
                              )) 
}