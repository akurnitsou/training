variable "access_key_id" {
}

variable "secret_access_key" {
}

variable "ssh_key" {
}

variable "db_user"{
  default  = "sonar"
}

variable "db_pass"{
  default  = "Sonar_sonaR"
}

variable "db_name"{
  default  = "sonar"
}

variable "db_user_env"{
  default  = "db_user"
}

variable "db_pass_env"{
  default  = "db_password"
}

variable "db_name_env"{
  default  = "realworld"
}

variable "region" {
  default  = "us-east-1"
}

variable "assume_role" {
  default = "arn:aws:iam::242906888793:role/AWS_Sandbox"
}

variable "session_name" {
  default = "AWS_Sandbox"
}

variable "vpc_network" {
  default = "172.16.0.0/16"
}

variable "public_subnets" {
  default = ["172.16.0.0/24", "172.16.1.0/24"]
}

variable "private_subnets" {
  default = ["172.16.2.0/24", "172.16.3.0/24"]
}

variable "db_subnets" {
  default = ["172.16.4.0/24", "172.16.5.0/24"]
}

variable "common_tags" {
  type = map(string)
  default = {
    project = "devops-training"
    client = "Coherent"    
    owner = "olegkurnitsov@coherentsolutions.com"
  }
}