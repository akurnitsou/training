# **Main description**

### Sonarqube

1. Add System settings. Edit “sysctl.conf” file: nano /etc/sysctl.conf
Add the following lines:
vm.max_map_count=262144  
fs.file-max=65536  
Don't forget reboot.  

2. Install JAVA on EC2  
"amazon-linux-extras install java-openjdk11 -y"  

3. Find default java path  
“update-alternatives --config java” it should looks like “/usr/lib/jvm/java-11-openjdk-11.0.7.10-4.amzn2.0.1.x86_64”  

4. Create env variables “JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.7.10-4.amzn2.0.1.x86_64”  

5. Add following lines to end of the file “/etc/bashrc”  
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.7.10-4.amzn2.0.1.x86_64/  
export JRE_HOME=/usr/lib/jvm/java-11-openjdk-11.0.7.10-4.amzn2.0.1.x86_64/jre  
PATH=$PATH:$HOME/bin:$JAVA_HOME/bin  

6. Run commands to download and prepare sonarqube:  
"wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-8.5.0.37579.zip",  
"sudo unzip sonarqube-8.5.0.37579.zip -d /opt",  
"sudo mv /opt/sonarqube-8.5.0.37579 /opt/sonarqube"  

7. Uncomment and fill valid data into file “/opt/sonarqube/conf/sonar.properties”
sonar.jdbc.username=  
sonar.jdbc.password=  
sonar.jdbc.url=jdbc:postgresql://localhost/sonar  

8. Create user and set password  
“useradd sonar”  
“passwd sonar”  

9. Change ownership on folder and files:  
“chown -R sonar:sonar /opt/sonarqube”  

10. Create new Systemd service, run “nano /etc/systemd/system/sonarqube.service”  
[Unit]  
Description=SonarQube service  
After=syslog.target network.target  
[Service]  
Type=forking  
ExecStart=/opt/sonarqube/bin/linux-x86-64/sonar.sh start  
ExecStop=/opt/sonarqube/bin/linux-x86-64/sonar.sh stop  
LimitNOFILE=65536  
LimitNPROC=4096  
User=sonar  
Group=sonar  
Restart=on-failure  
[Install]  
WantedBy=multi-user.target  
  
11. Run service:  
“systemctl daemon-reload”  
“systemctl enable sonarqube.service”  
“systemctl start sonarqube.service”  
  
12. Check logs “tail -f /opt/sonarqube/logs/sonar.log”  

### Nexus

Prerequirements:  
Add System settings. Edit “sysctl.conf” file: nano /etc/sysctl.conf
Add the following lines:
vm.max_map_count=262144  
fs.file-max=65536  
1. Update packages, run "yum update -y".
2. Install necessary packages, run "yum install java-1.8.0-openjdk-devel -y".
3. Create directory for nexus, run  "mkdir /app && cd /app".
4. Download latest nexus version, run "wget -O nexus.tar.gz https://download.sonatype.com/nexus/3/latest-unix.tar.gz".
5. Untar the downloaded file, run "tar -xvf nexus.tar.gz".
6. Rename the untared file to nexus, run "mv nexus-3* nexus".
7. Create a new user, run "adduser nexus".
8. Change the ownership, run "chown -R nexus:nexus /app/nexus".
9. Change the ownership, run "chown -R nexus:nexus /app/sonatype-work".
10. Open /app/nexus/bin/nexus.rc file and uncommented parameter, set as run_as_user="nexus".
11. Create a nexus systemd unit file, run "nano /etc/systemd/system/nexus.service".
12. Add the following contents to the unit file.  
[Unit]  
Description=nexus service  
After=network.target  
[Service]  
Type=forking  
LimitNOFILE=65536  
User=nexus  
Group=nexus  
ExecStart=/app/nexus/bin/nexus start  
ExecStop=/app/nexus/bin/nexus stop  
User=nexus  
Restart=on-abort  
[Install]  
WantedBy=multi-user.target  
13. Execute the following command to add nexus service to boot, run "chkconfig nexus on".
14. Start the Nexus service, run "systemctl start nexus".

### Jenkins

1. sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
2. sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
3. sudo yum upgrade
4. sudo yum install jenkins java-1.8.0-openjdk-devel
5. sudo systemctl daemon-reload

### Frontend script

Prerequirements: Install node.js on Jenkins before build.
Groovy build script:
```
# !groovy

pipeline {
    agent any
    options {
        skipStagesAfterUnstable()
    }
    stages {
        stage('Clone repository') {
            steps {
                git 'https://github.com/vitamin-b12/devops-training-project-frontend.git'
            }
        }
        stage('Replace backend URL') {
            steps {
                sh "sed -i 's/conduit.productionready.io\\/api/backend.okurnitsov.test.coherentprojects.net/g' src/agent.js"
            }
        }
        stage('Sonarqube'){
            environment {
                scannerHome = tool 'sonarqube_scaner'
            }
            steps {
                withSonarQubeEnv('sonarqube') {
                     sh "${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=frontend -Dsonar.sources=."
                }
            }
        }
        stage("Quality Gate") {
            steps {
                sleep(3)
                timeout(time: 3, unit: 'MINUTES') {
                    script  {
                        def qg = waitForQualityGate()
                        if (qg.status != 'OK') {
                            error "Pipeline aborted due to quality gate failure: ${qg.status}"
                        }
                    }
                }
            }
        }
        stage('Build') {
            steps {
                sh "npm install"
                sh "npm run build"
            }
        }
        stage('Push to Nexus') {
            environment {
                BUILD_DATE = sh(returnStdout: true, script: "date -u +'%d_%m_%Y_%H_%M_%S'").trim()
            }
            steps {
                sh "zip -r build-${BUILD_ID}.zip build/*"
                script {
                    dir('.') {
                    def artifact_name = "build-${BUILD_ID}"
                    nexusArtifactUploader artifacts: [[artifactId: 'build', file: "${artifact_name}.zip", type: 'zip']],
                        credentialsId: 'jenkins',
                        groupId: 'devops-training',
                        nexusUrl: '${NEXUS_URL}',
                        nexusVersion: 'nexus3',
                        protocol: 'https',
                        repository: '${NEXUS_FRONT}',
                        version: "${BUILD_DATE}"
                    }
                }
            }
        }
    }
    post {
        always {
            cleanWs()
        }
        success{
            echo " ---=== SUCCESS ===---"
        }
        failure{
            echo " ---=== FAILURE ===---"
        }
    }
}
```

### Backend script

Prerequirements: Run docker container with DB on Jenkins, need for tests.
```
# !groovy

pipeline {
    agent any
    options {
        skipStagesAfterUnstable()
    }
    stages {
        stage('Clone repository') {
            steps {
                git 'https://github.com/vitamin-b12/devops-training-project-backend.git'
            }
        }
        stage('Update gradle file') {
            steps {
                sh "sed -i '65s/.*/ /' build.gradle"
            }
        }
        stage('Test') {
            environment{
                DB_USERNAME="${DB_USERNAME}"
                DB_PASSWORD="${DB_PASSWORD}"
                DB_URL="${DB_URL}"
                DB_PORT="${DB_PORT}"
                DB_NAME="${DB_NAME}"
            }
            steps {
                sh "./gradlew test"
            }
        }
        stage('Build') {
            steps {
                sh "./gradlew build -x test"
            }
        }
        stage('Sonarqube'){
            environment {
                scannerHome = tool 'sonarqube_scaner'
            }
            steps {
                withSonarQubeEnv('sonarqube') {
                     sh "./gradlew -x test sonarqube -Dsonar.projectKey=backend"
                }
            }
        }
        stage("Quality Gate") {
            steps {
                sleep(5)
                timeout(time: 5, unit: 'MINUTES') {
                    script  {
                        def qg = waitForQualityGate()
                        if (qg.status != 'OK') {
                            error "Pipeline aborted due to quality gate failure: ${qg.status}"
                        }
                    }
                }
            }
        }
        stage('Push to Nexus') {
            environment {
                BUILD_DATE = sh(returnStdout: true, script: "date -u +'%d_%m_%Y_%H_%M_%S'").trim()
            }
            steps {
                sh "mv build/libs/*.jar backend-${BUILD_ID}.jar"
                script {
                    dir('.') {
                    def artifact_name = "backend-${BUILD_ID}"
                    nexusArtifactUploader artifacts: [[artifactId: 'build', file: "${artifact_name}.jar", type: 'jar']],
                        credentialsId: 'jenkins',
                        groupId: 'devops-training',
                        nexusUrl: '${NEXUS_URL}',
                        nexusVersion: 'nexus3',
                        protocol: 'https',
                        repository: '${NEXUS_BACK}',
                        version: "${BUILD_DATE}"
                    }
                }
            }
        }
    }
    post {
        always {
            cleanWs()
        }
        success{
            echo " ---=== SUCCESS ===---"
        }
        failure{
            echo " ---=== FAILURE ===---"
        }
    }
}
```