output "bastion_host_ip_address" {
  value       = aws_instance.okurnitsov_environment_bastion_instance.public_ip
  description = "The ip address of the bastion host"
}

output "db_endpoint_env" {
  value       = aws_rds_cluster.okurnitsov_environment_rds_cluster.endpoint
  description = "The ip address of the bastion host"
}

output "jenkins_ip" {
  value       = aws_instance.okurnitsov_environment_jenkins_instance.private_ip
}

output "public_ip" {
  value       = aws_instance.okurnitsov_environment_empty_instance.*.public_ip
}

output "private_ip" {
  value       = aws_instance.okurnitsov_environment_empty_instance.*.private_ip
}
