### --- === Security groups === --- ###

resource "aws_security_group" "okurnitsov_environment_external_ssh_access" {
  name          = "ssh-access-from-minsk"
  description   = "Allow SSH traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["80.94.174.80/29", "86.57.155.176/29", "212.98.191.192/29", "212.98.191.204/30", "212.98.168.16/28"]
  }
  egress {
    from_port   = 0 #Allow all traffic
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-environment-external-ssh-access"
                  ))
}

resource "aws_security_group" "okurnitsov_environment_internal_ssh_access" {
  name          = "ssh-access"
  description   = "Allow SSH traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_environment_vpc.cidr_block]
  }
  egress {
    from_port   = 0 #Allow all traffic
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-environment-internal-ssh-access"
                  ))
}

resource "aws_security_group" "okurnitsov_environment_web_access" {
  name          = "okurnitsov-environment-web-access"
  description   = "Allow web traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0 
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-environment-web-access"
                  ))
}

resource "aws_security_group" "okurnitsov_environment_db_access" {
  name          = "okurnitsov-environment-db-access"
  description   = "Allow db traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_environment_vpc.cidr_block]
  }
  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_environment_vpc.cidr_block]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-environment-db-access"
                  ))
}

resource "aws_security_group" "okurnitsov_db_access" {
  name          = "okurnitsov-db-access"
  description   = "Allow db traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_environment_vpc.cidr_block]
  }
  egress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.okurnitsov_environment_vpc.cidr_block]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-db-access"
                  ))
}

resource "aws_security_group" "okurnitsov_environment_icmp_access" {
  name          = "okurnitsov-environment-icmp-access"
  description   = "Allow icmp traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = "-1"
    to_port     = "-1"
    protocol    = "icmp"
    cidr_blocks = [aws_vpc.okurnitsov_environment_vpc.cidr_block]
  }
  egress {
    from_port   = "-1"
    to_port     = "-1"
    protocol    = "icmp"
    cidr_blocks = [aws_vpc.okurnitsov_environment_vpc.cidr_block]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-environment-icmp-access"
                  ))
}

resource "aws_security_group" "okurnitsov_environment_jenkins_access" {
  name          = "okurnitsov-jenkins-access"
  description   = "Allow backend traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-jenkins-access"
                  ))
}

resource "aws_security_group" "okurnitsov_environment_sonarqube_access" {
  name          = "okurnitsov-sonarqube-access"
  description   = "Allow backend traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-sonarqube-access"
                  ))
}

resource "aws_security_group" "okurnitsov_environment_nexus_access" {
  name          = "okurnitsov-nexus-access"
  description   = "Allow backend traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-nexus-access"
                  ))
}

resource "aws_security_group" "okurnitsov_frontend_access" {
  name          = "okurnitsov-frontend-access"
  description   = "Allow frontend traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-frontend-access"
                  ))
}

resource "aws_security_group" "okurnitsov_backend_access" {
  name          = "okurnitsov-backend-access"
  description   = "Allow backend traffic"
  vpc_id        = aws_vpc.okurnitsov_environment_vpc.id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = merge(var.common_tags, map(
                    "Name", "okurnitsov-backend-access"
                  ))
}