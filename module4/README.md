### This repository for training ###

This branch for module4.

## Task 1 ##
 1. Run "vagrant up ubuntu"
 2. Run "vagrant up centos1 && vagrant up centos2"
 3. Run "ssh vagrant@centos1 -p 2222 -i /home/vagrant/.ssh/main_ssh_key" to connect centos1 VM
 4. Run "ssh vagrant@centos2 -p 2222 -i /home/vagrant/.ssh/main_ssh_key" to connect centos2 VM


## Task 2 ##
 1. Run 'vagrant up"

## Task 3 ##
 1. Install plugin, run "vagrant plugin install vagrant-vbguest"
 2. Use key pair generated for task task1
 3. Connect to vm ubuntu, run "vagrant ssh ubuntu"
 4. Run "eval $(ssh-agent -s)" and "ssh-add /home/vagrant/.ssh/main_ssh_key". After that you can user commnad "ssh vagrant@centos1"
 5. Run "sudo ifdown eth0" and "sudo systemctl restart network" on centos VMs, after that you can ping 8.8.8.8
 6. Check iptables rules and interfaces configs into "vm_output" folder