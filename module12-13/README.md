# **Main description**

## Ansible role creation task

Use Ansible Galaxy for roles downloading. Run followed commands:  

1. ansible-galaxy install cloudalchemy.node-exporter
2. ansible-galaxy install cloudalchemy.prometheus  
3. ansible-galaxy install geerlingguy.logstash
4. ansible-galaxy install diodonfrost.java
5. ansible-galaxy install geerlingguy.elasticsearch
6. ansible-galaxy install geerlingguy.kibana

## Install needed roles on declared hosts

1. Run ansible-playbook log_instance.yml and don't forget port 9100
2. Run ansible-playbook logback_exporter.yml
3. Install Filebeat on frontend instance and configure [link](https://logit.io/sources/configure/nginx)  
   Update config /etc/filebeat/filebeat.yml  

```
   filebeat.inputs:
        - type: log
          enabled: true
          paths:
                - /var/log/nginx/*.log

   filebeat.config.modules:
        path: ${path.config}/modules.d/*.yml
        reload.enabled: true
        reload.period: 10s

   output.logstash:
        hosts: ["172.16.2.252:5044"]
```

4. Configure logstash on log instance, create config files for frontend and backend and store it in "/etc/logstash/conf.d"

Frontend

```
input {
  beats {
    port => 5044
    ssl => false
  }
}

output {
  elasticsearch {
    hosts => ["http://localhost:9200"]
    index => "frontend-%{+YYYY.MM.dd}"
  }
}
```

Backend  

```
input {
  tcp {
    port => 4561
    codec => json
  }
}

output {
  elasticsearch {
    hosts => ["http://localhost:9200"]
    index => "backend-%{+YYYY.MM.dd}"
  }
}
```

   Uncommented line "http.host" and set "http.host: 0.0.0.0"

5. Configure Kibana
   Open "Stack Manager" and add existed stream from filebeat, use mask "filebeat-*". Filebeat should be exist. Add the same mask for frontend and backend.
   ![Filebeat](kibana_filebeat.png)
   Kibana result  
   ![Kibana](kibana_result.png)

## Module 13

  Use Ansible Galaxy for roles downloading. Run followed commands:

1. Run ansible-galaxy install cloudalchemy.blackbox-exporter
2. Run ansible-galaxy install cloudalchemy.grafana
3. Run ansible-playbook log_instance.yml and finish configuration.  
![Grafana](grafana.png)
4. Configure prometheus, edit "/etc/prometheus/prometheus.yml" [link](https://devconnected.com/how-to-install-and-configure-blackbox-exporter-for-prometheus/)

  ```
  global:
    evaluation_interval: 15s
    scrape_interval: 15s
    scrape_timeout: 10s

  rule_files:
    - /etc/prometheus/rules/*.rules

  scrape_configs:
    - job_name: prometheus
      metrics_path: /metrics
      static_configs:
      - targets: ["localhost:9090"]
    - file_sd_configs:
      - files:
        - /etc/prometheus/file_sd/node.yml
      job_name: node

    - job_name: node_exporter
      static_configs:
      - targets: ["172.16.2.31:9100"] # instance with frontend + backend
  
    - job_name: 'blackbox'
      metrics_path: /probe
      params:
        module: ["http_2xx"]
      static_configs:
        - targets:
          - "https://grafana-okurnitsov.test.coherentprojects.net"
          - "https://prometheus.okurnitsov.test.coherentprojects.net"
        relabel_configs:
        - source_labels: [__address__]
          target_label: __param_target
        - source_labels: [__param_target]
          target_label: instance
        - target_label: __address__
          replacement: 127.0.0.1:9115  # The blackbox exporter's real hostname:port.  
  ```

  5. Configure Grafana, import boards 5345 and 1860.
     Board 1860 no need extra configuration.
     On Board 5345 you shoud replace "target", follow screenshots
Grafana target
![Grafana target](grafana_blackbox.png)
Grafana query target (configure for all panels)  
![Grafana query target](grafana_blackbox3.png)
Grafana result  
![Grafana result](grafana_blackbox2.png)
Prometheus query example
![Prometheus query](prometheus_query.png)