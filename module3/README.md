### This repository for training ###

This branch for module3.

## Task 1 and Task 2 ##
1. Run 'vagrant up" for solution

## Task 3 #
1. Enable additional plugin, run command "vagrant plugin install vagrant-persistent-storage"
2. Run 'vagrant up" for solution

## Task 4 #
1. Run 'vagrant up" for solution

## Task 5-8 #
1. Configure 50-vagrant.yaml for internal network and 50-cloud-init.yaml for public network
2. Prepare config.ru and production.rb files for Puma
3. Prepare syncthing.service for simple service solution
4. Run 'vagrant up" for solutions