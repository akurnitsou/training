#!/bin/bash

echo "### ---=== Script started ===--- ###"

### 1------------------------------------------------------------------ ###

echo "export ISSOFT_VAR=ISSOFT_VAR_VALUE" >> /etc/environment
source /etc/environment

### 2------------------------------------------------------------------ ###
HOME_DIR=$(pwd)
DIRECTORY1=current
### Control will enter here if $DIRECTORY1 doesn't exist. ###
if [ ! -d "$DIRECTORY1" ]
  then
    mkdir $DIRECTORY1
    echo "Folder $DIRECTORY1 was created"
    cp /opt/share/ubuntu-bionic-18.04-cloudimg-console.log $HOME_DIR/$DIRECTORY1/ubuntu-bionic-18.04-cloudimg-console.log && chmod -R 755 $HOME_DIR/$DIRECTORY1/ubuntu-bionic-18.04-cloudimg-console.log
  else
    echo "Folder $DIRECTORY1 is exist"
fi

DIRECTORY2=new
### Control will enter here if $DIRECTORY2 doesn't exist. ###
if [ ! -d "$DIRECTORY2" ]
  then
    mkdir $DIRECTORY2
    echo "Folder $DIRECTORY2 was created"
    cp /opt/share/ubuntu-bionic-18.04-cloudimg-console.log $HOME_DIR/$DIRECTORY2/ubuntu-bionic-18.04-cloudimg-console.log && chmod -R 755 $HOME_DIR/$DIRECTORY2/ubuntu-bionic-18.04-cloudimg-console.log && touch -d 20191009 $HOME_DIR/$DIRECTORY2/ubuntu-bionic-18.04-cloudimg-console.log
  else
    echo "Folder $DIRECTORY2 is exist"
fi

DIRECTORY3=old
### Control will enter here if $DIRECTORY2 doesn't exist. ###
if [ ! -d "$DIRECTORY3" ]
  then
    mkdir $DIRECTORY3
    echo "Folder $DIRECTORY3 was created"
    cp /opt/share/ubuntu-bionic-18.04-cloudimg-console.log $HOME_DIR/$DIRECTORY3/ubuntu-bionic-18.04-cloudimg-console.log
  else
    echo "Folder $DIRECTORY3 is exist"
fi

tar -czvf $HOME_DIR/folders.tar new old current

### 3------------------------------------------------------------------ ###
echo "export ISSOFT_VAR=https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.5.0-amd64-netinst.iso" >> /etc/environment
source /etc/environment
wget --tries=10 -c $ISSOFT_VAR

echo "export ISSOFT_VAR=https://bitbucket.org/akurnitsou/training/get/master.zip" >> /etc/environment
source /etc/environment
curl $ISSOFT_VAR --output repo.zip

echo "export ISSOFT_VAR=ISSOFT_VAR_VALUE" >> /etc/environment
source /etc/environment

echo "### ---=== Script comleted ===--- ###"