#!/bin/bash

echo "### ---=== Script started ===--- ###"

echo "Input user name:"
read typed
if [ -z "$typed" ]
  then
    echo "No argument supplied"
    exit 1
fi
if id $typed > /dev/null 2>&1
then
  echo "User exist, you can't create user $typed"
else
  echo "User doesn't exist"
  adduser $typed --gecos ""
  echo "User $typed created"
fi

echo "### ---=== Script comleted ===--- ###"