#!/bin/sh
mysql -e "CREATE USER '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASS';"
mysql -e "CREATE USER '$DB_USER'@'%' IDENTIFIED BY '$DB_PASS';"
mysql -e "GRANT ALL PRIVILEGES ON * . * TO '$DB_USER'@'localhost';"
mysql -e "GRANT ALL PRIVILEGES ON * . * TO '$DB_USER'@'%';"
mysql -e "FLUSH PRIVILEGES;"
mysql -e "CREATE DATABASE $DB_NAME;"