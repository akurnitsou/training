### --- === VPC === --- ###

data "aws_availability_zones" "available_zones_in_current_region" {
  state                   = "available"
}


resource "aws_vpc" "okurnitsov_vpc" {
  cidr_block              = var.vpc_network
  enable_dns_hostnames    = true
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-vpc"
                          ))  
}

### --- === Subnets === --- ###

resource "aws_subnet" "okurnitsov_private_subnets" {
  count                   = length(var.private_subnets)
  vpc_id                  = aws_vpc.okurnitsov_vpc.id
  cidr_block              = var.private_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags = merge(var.common_tags, map(
    "Name", "okurnitsov-private-subnets-${count.index+1}"
  ))
}

resource "aws_subnet" "okurnitsov_public_subnets" {
  count                   = length(var.public_subnets)
  vpc_id                  = aws_vpc.okurnitsov_vpc.id
  cidr_block              = var.public_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-public-subnets-${count.index+1}"
                          ))
}

resource "aws_subnet" "okurnitsov_db_subnets" {
  count                   = length(var.db_subnets)
  vpc_id                  = aws_vpc.okurnitsov_vpc.id
  cidr_block              = var.db_subnets[count.index]
  availability_zone       = data.aws_availability_zones.available_zones_in_current_region.names[count.index]
  map_public_ip_on_launch = "true"
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-db-subnets"
                          ))
}

### --- === Internet gateway === --- ###

resource "aws_internet_gateway" "okurnitsov_igw" {
  vpc_id                  = aws_vpc.okurnitsov_vpc.id
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-igw"
                          ))
}

### --- === Route tables === --- ###

resource "aws_route_table" "okurnitsov_vpc_public_route_table" {
  vpc_id = aws_vpc.okurnitsov_vpc.id
  route {
    cidr_block            = "0.0.0.0/0"
    gateway_id            = aws_internet_gateway.okurnitsov_igw.id
  }
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-vpc-public-route-table"
                          ))
}

resource "aws_route_table" "okurnitsov_vpc_private_route_table" {
  vpc_id                  = aws_vpc.okurnitsov_vpc.id
  route {
    cidr_block            = "0.0.0.0/0"
    instance_id           = aws_instance.okurnitsov_nat_instance.id
  }
  depends_on              = [aws_instance.okurnitsov_nat_instance]
  tags                    = merge(var.common_tags, map(
                              "Name", "okurnitsov-vpc-private-route-table"
                          ))
}

### --- === Route tables association === --- ###

resource "aws_route_table_association" "associate_public_subnets_with_public_route_table1" {
  subnet_id      = aws_subnet.okurnitsov_public_subnets[0].id
  route_table_id = aws_route_table.okurnitsov_vpc_public_route_table.id
}

resource "aws_route_table_association" "associate_public_subnets_with_public_route_table2" {
  subnet_id      = aws_subnet.okurnitsov_public_subnets[1].id
  route_table_id = aws_route_table.okurnitsov_vpc_public_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table1" {
  subnet_id      = aws_subnet.okurnitsov_private_subnets[0].id
  route_table_id = aws_route_table.okurnitsov_vpc_private_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table2" {
  subnet_id      = aws_subnet.okurnitsov_private_subnets[1].id
  route_table_id = aws_route_table.okurnitsov_vpc_private_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table3" {
  subnet_id      = aws_subnet.okurnitsov_db_subnets[0].id
  route_table_id = aws_route_table.okurnitsov_vpc_private_route_table.id
}

resource "aws_route_table_association" "associate_private_subnets_with_private_route_table4" {
  subnet_id      = aws_subnet.okurnitsov_db_subnets[1].id
  route_table_id = aws_route_table.okurnitsov_vpc_private_route_table.id
}

# ### --- === Route 53 === --- ###

# data "aws_route53_zone" "check_zone_id" {
#   name         = "test.coherentprojects.net."
# }

# # resource "aws_route53_record" "okurnitsov_route53_zone" {
# #   zone_id = data.aws_route53_zone.check_zone_id.zone_id
# #   name    = "okurnitsov.${data.aws_route53_zone.check_zone_id.name}"
# #   type    = "A"
# #   ttl     = "300"
# #   records = [aws_instance.okurnitsov_frontend_instance.private_ip]
# # }