### --- === EC2 instances === --- ###

resource "aws_key_pair" "okurnitsov_key_pair" {
  key_name                    = "okurnitsov-key-pair"
  public_key                  = var.ssh_key
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-nat-instance"
                              )) 
}

### --- === NAT instances === --- ###

resource "aws_instance" "okurnitsov_nat_instance" {
  ami                         = "ami-00a9d4a05375b2763"
  instance_type               = "t2.nano"
  subnet_id                   = aws_subnet.okurnitsov_public_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnutsov_web_access.id, aws_security_group.okurnutsov_internal_ssh_access.id, aws_security_group.okurnutsov_icmp_access.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair.id
  depends_on                  = [aws_security_group.okurnutsov_external_ssh_access]
  source_dest_check           = "false"
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-nat-instance"
                              )) 
}

### --- === BASTION instances === --- ###

resource "aws_instance" "okurnitsov_bastion_instance" {
  ami                         = "ami-032930428bf1abbff"
  instance_type               = "t2.nano"
  subnet_id                   = aws_subnet.okurnitsov_public_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnutsov_external_ssh_access.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair.id
  lifecycle {
    prevent_destroy           = "true"
  }
  depends_on                  = [aws_security_group.okurnutsov_external_ssh_access]
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-bastion-instance"
                              )) 
}

### --- === db instances === --- ###

resource "aws_instance" "okurnitsov_db_instance" {
  ami                         = "ami-49fa8e5f"
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.okurnitsov_db_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnutsov_internal_ssh_access.id,aws_security_group.okurnutsov_db_access.id, aws_security_group.okurnutsov_icmp_access.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair.id
  associate_public_ip_address = "false"
  user_data                   = data.template_file.db.rendered
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-db-instance"
                              )) 
}

### --- === front instances === --- ###

resource "aws_instance" "okurnitsov_frontend_instance" {
  ami                         = "ami-0947d2ba12ee1ff75"
  instance_type               = "t3a.micro"
  subnet_id                   = aws_subnet.okurnitsov_private_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnutsov_web_access.id, aws_security_group.okurnutsov_icmp_access.id, aws_security_group.okurnutsov_internal_ssh_access.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair.id
  associate_public_ip_address = "false"
  depends_on                  = [aws_instance.okurnitsov_backend_instance]
  user_data                   = data.template_file.frontend.rendered
      connection {
      type                = "ssh"
      agent               = false
      user                = "ec2-user"
      host                = self.private_ip
      private_key         = file("issoft_ssh_key")
      bastion_host        = aws_instance.okurnitsov_bastion_instance.public_ip
      bastion_private_key = file("issoft_ssh_key")
    }
  provisioner "remote-exec" {
      inline = [
        "REACT_APP_API_URL = ${aws_instance.okurnitsov_backend_instance.private_ip}",
        "API_URL = ${aws_instance.okurnitsov_backend_instance.private_ip}",
    ]
  }
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-frontend-instance"
                              )) 
}

### --- === back instances === --- ###

resource "aws_instance" "okurnitsov_backend_instance" {
  ami                         = "ami-0947d2ba12ee1ff75"
  instance_type               = "t3a.small"
  subnet_id                   = aws_subnet.okurnitsov_private_subnets[0].id
  vpc_security_group_ids      = [aws_security_group.okurnutsov_internal_ssh_access.id, aws_security_group.okurnutsov_icmp_access.id]
  key_name                    = aws_key_pair.okurnitsov_key_pair.id
  associate_public_ip_address = "false"
  user_data                   = data.template_file.backend.rendered
  depends_on                  = [aws_instance.okurnitsov_db_instance]
    connection {
      type                = "ssh"
      agent               = false
      user                = "ec2-user"
      host                = self.private_ip
      private_key         = file("issoft_ssh_key")
      bastion_host        = aws_instance.okurnitsov_bastion_instance.public_ip
      bastion_private_key = file("issoft_ssh_key")
    }
  provisioner "remote-exec" {
      inline = [
        "DB_URL = ${aws_instance.okurnitsov_db_instance.private_ip}",
        "DB_USERNAME = ${var.db_user}",
        "DB_PASSWORD = ${var.db_user}",
        "DB_NAME = ${var.db_name}",
        "DB_PORT = ${var.db_port}",
    ]
  }
  tags                        = merge(var.common_tags, map(
                                  "Name", "okurnitsov-backend-instance"
                                )) 
}

data template_file backend {
  template                    = file("backend-init.yaml")
  vars                        = {
                                  DB_URL = aws_instance.okurnitsov_db_instance.private_ip
                                  DB_USERNAME = var.db_user
                                  DB_PASSWORD = var.db_pass
                                  DB_NAME = var.db_name
                                  DB_PORT = var.db_port
  }
}

data template_file frontend {
  template                    = file("frontend-init.yaml")
  vars                        = {
                                  BACKEND_URL = aws_instance.okurnitsov_backend_instance.private_ip
  }
}

data template_file db {
  template                    = file("db-init.yaml")
  vars                        = {
                                  DB_USERNAME = var.db_user
                                  DB_PASSWORD = var.db_pass
                                  DB_NAME = var.db_name
  }
}