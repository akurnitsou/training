### **Main description** ###

Preparation: Install Active Choices Plugin and follow this [link](https://medium.com/@g4b1s/dynamically-list-git-branches-in-jenkins-job-parameter-3e6e849f8a98)

## Frontend ##
 1. Frontend repository [link](https://github.com/nobumori/devops-training-project-frontend).  
 2. Main development branch is "develop".  
 3. Jenkins script stored in "devops" branch. Build is papametrized.   
Frontend screen settings part1  
![Frontend scan](frontend1.png)  
Frontend screen settings part2
![Frontend scan](frontend2.png)

## Backend ##
 1. Backend repository [link](https://github.com/nobumori/devops-training-project-backend).
 2. Main development branch is "develop"  
 3. Jenkins script stored in "devops" branch. Build is papametrized.  
Backend screen settings part1  
![Backend scan](backend1.png)  
Backend screen settings part2  
![Backend scan](backend2.png)