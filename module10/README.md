# **Main description**

## Ansible role creation task

Use Ansible Galaxy for roles downloading. Run followed commands:  

1. ansible-galaxy install diodonfrost.java
2. ansible-galaxy install geerlingguy.jenkins  
3. ansible-galaxy install lrk.sonarqube  
4. remove folloving lines from ".ansible/roles/lrk.sonarqube/meta/main.yml"  
        "dependencies:"  
        "- geerlingguy.java"
5. ansible-galaxy install pogosoftware.nexus3_oss

## CodePippeline task

Don't forget upload imagedefinitions.json into S3 bucket.  
Data for imagedefinitions.json generated in CodeBuild.
Frontend CodePipeline result.
![Frontend](codepipeline_front.png)
Backend CodePipeline result.
![Backend](codepipeline_back.png)

## Ansible role deployment task  

1. Don't forget set permissions on key file for jenkins user.
2. Use Jenkins to deploy on selected host.
3. Playbook stored in repo. If you want store it on ansible host, please store it in jenkins user space. 